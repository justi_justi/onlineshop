package frompythontojava.onlineshop.part1;

import java.util.ArrayList;

public class Product {
    String name;
    float defaultPrice;
    ProductCategory productCategory;
    int ID;
    static int counter = 0;
    static ArrayList<Product> productList = new ArrayList<>();

    public Product(String name, float defaultPrice, ProductCategory productCategory){
        this.name = name;
        this.defaultPrice = defaultPrice;
        this.productCategory = productCategory;
        this.ID = counter;
        counter++;
        productList.add(this);
    }

    public String getName() {
        return name;
    }

    public float getDefaultPrice() {
        return defaultPrice;
    }

    public AvailableProductIterator getIterator(){
        return new AvailableProductIterator();
    }

    @Override
    public String toString() {
        if (productCategory instanceof FeaturedProductCategory){
            return String.format("%s, ID:%d, name:%s, defaultPrice:%.2f |", productCategory.name, ID, name, defaultPrice);
        }
        return String.format("ID:%d, name:%s, defaultPrice:%.2f |", ID, name, defaultPrice);
    }

    public static ArrayList getAllProducts(){
        return productList;

    }
    public static ArrayList getAllProductsBy(ProductCategory myProductCategory){
        ArrayList<Product> productListByCategory = new ArrayList<>();
        for (Product product : productList){
            if(product.productCategory == myProductCategory){
            productListByCategory.add(product);
            }
        }
        return productListByCategory;
    }

    private class AvailableProductIterator implements frompythontojava.onlineshop.part1.Iterator {

        int index;

        @Override
        public boolean hasNext() {

            if(index < productList.size()) {
                return true;
            }
            return false;
        }

        @Override
        public Product next() {

            if(this.hasNext()){
                return productList.get(index++);
            }
            return null;
        }
    }
}
