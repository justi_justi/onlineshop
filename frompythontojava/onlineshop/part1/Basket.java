package frompythontojava.onlineshop.part1;
import java.util.ArrayList;

public class Basket {
    ProductIterator iterator;
    ArrayList<Product> productList = new ArrayList<>();


    public ArrayList<Product> getProductList() {
        return productList;
    }

    public ProductIterator getIterator(){
        return new ProductIterator();
    }
    public void addProduct(Product product){
        productList.add(product);
    }

    public boolean removeProduct(Product product) {
        return this.productList.remove(product);
    }

    private class ProductIterator implements frompythontojava.onlineshop.part1.Iterator {

        int index;

        @Override
        public boolean hasNext() {

            if(index < productList.size()) {
                return true;
            }
            return false;
        }

        @Override
        public Product next() {

            if(this.hasNext()){
                return productList.get(index++);
            }
            return null;
        }
    }
    }
