package frompythontojava.onlineshop.part1;
import java.util.Date;

public class FeaturedProductCategory extends ProductCategory {
    Date expirationDate;

    public FeaturedProductCategory(String name, Date expirationDate) {
        super(name);
        this.expirationDate = expirationDate;
        categoryCounter++;


    }
}
