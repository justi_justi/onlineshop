package frompythontojava.onlineshop.part1;
import java.util.ArrayList;

public class ProductCategory {
    String name;
    int categoryID;
    static int categoryCounter = 0;
    static ArrayList<ProductCategory> categoryList = new ArrayList<>();


    public ProductCategory(String name){
        this.name = name;
        this.categoryID = this.categoryCounter;
        categoryList.add(this);
        categoryCounter ++;

    }
    public String toString() {

        return String.format("ID:%d, name:%s|", this.categoryID, this.name);
    }

    public String getName(){
        return this.name;
    }

    public static ArrayList getAllCategories(){
        return categoryList;
    }
    public static ProductCategory getCategoryByID(int myID){
        for (ProductCategory myCategory : categoryList){
            if(myCategory.categoryID == myID){
                return myCategory;
            }

    }
    return null;
}
    public static ProductCategory getCategoryByName(String myCategoryName){
        for (ProductCategory myCategory : categoryList){
            if(myCategory.getName().equals(myCategoryName)){
                return myCategory;
            }

        }
        return null;
    }
}
