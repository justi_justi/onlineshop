package frompythontojava.onlineshop.part3;
import frompythontojava.onlineshop.part1.*;
import frompythontojava.onlineshop.part2.*;


import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;


public class Application {

    public static void main(String[] args) {
        Basket myBasket = new Basket();
        boolean weAreShopping = true;
        while (weAreShopping) {
            System.out.println();
            System.out.println("It's shopping time!");
            System.out.println("1 - Create a new product.\n"
                    + "2 - Create a new product category.\n"
                    + "3 - Add product to the basket.\n"
                    + "4 - See all products in the basket\n"
                    + "5 - Remove a product from basket\n"
                    + "6 - Get the list of all available products\n"
                    + "7 - Get the list of products from a specific category\n"
                    + "8 - Check availability of a specific product\n"
                    + "9 - Pay for your order");

         int option= getInput();
         if(option == 1){
             createNewProduct();
         } else if(option == 2){
             System.out.println("Current categories:");
             System.out.println(ProductCategory.getAllCategories());
                 System.out.println("1 -Create a basic product category\n"
                 + "2 - Create a featured product category");
                 int choice = getIfFeatured();
                 if (choice == 1){
                     createNewCategory();
                 }
                 else {
                     createNewFeaturedCategory();
                 }
         } else if(option == 3){
             addProductToBasket(myBasket);

         } else if(option == 4){
             seeYourBasket(myBasket);
         } else if(option == 5){
             removeProductFromBasket(myBasket);
         } else if(option == 6){
             System.out.println(getListOfAllProducts());
         } else if(option == 7){
             System.out.println(getByCategory());
         } else if(option == 8){
             System.out.println(checkAvailability());
         } else if(option == 9) {
             payForOrder(myBasket);
         }


         }

         }



    public static int getInput() {
        int option = 0;
        boolean optionNotSelected = true;
        while (optionNotSelected) {
            System.out.println("Select a number: ");
            Scanner scanner = new Scanner(System.in);
            try {
                option = scanner.nextInt();
                if (option>0 && option<10) {
                optionNotSelected = false;}
                else{
                    System.out.println("Invalid input");
                }
            } catch (java.util.InputMismatchException e) {
                System.out.println("Invalid input!");
            }
        }
        return option;
    }

    public static float getPrice() {
        float price = 0;
        boolean optionNotSelected = true;
        while (optionNotSelected) {
            System.out.println("Price: ");
            Scanner scanner = new Scanner(System.in);
            try {
                price = scanner.nextFloat();
                optionNotSelected = false;
            } catch (java.util.InputMismatchException e) {
                System.out.println("Invalid input!");
            }
        }
        return price;
    }

    public static int getCategoriesIDInput() {
        int option = 0;
        boolean optionNotSelected = true;
        while (optionNotSelected) {
            System.out.println("ID of chosen category: ");
            Scanner scanner = new Scanner(System.in);
            try {
                option = scanner.nextInt();
                if (option>-1 && option < ProductCategory.getAllCategories().size()) {
                    optionNotSelected = false;}
                else{
                    System.out.println("Invalid input");
                }
            } catch (java.util.InputMismatchException e) {
                System.out.println("Invalid input!");
            }
        }
        return option;
    }

    public static void createNewProduct(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Name: ");
        String name = scanner.nextLine();
        float price = getPrice();
        System.out.println("Current categories:");
        System.out.println(ProductCategory.getAllCategories());
        int categoryID = getCategoriesIDInput();
        ProductCategory myCategory = ProductCategory.getCategoryByID(categoryID);
        Product newProduct = new Product(name, price, myCategory);
        System.out.println("Product created successfully");

    }

    public static ArrayList getListOfAllProducts(){
        return Product.getAllProducts();

    }

    public static void createNewCategory(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Name: ");
        String name = scanner.nextLine();
        ProductCategory newCategory = new ProductCategory(name);
        System.out.println("Product category created successfully");


    }
    public static void createNewFeaturedCategory(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Name: ");
        String name = scanner.nextLine();
        Date myDate = new Date();
        ProductCategory newCategory = new FeaturedProductCategory(name, myDate);
        System.out.println("Product category created successfully");

    }

    public static int getIfFeatured(){
        int option = 0;
        boolean optionNotSelected = true;
        while (optionNotSelected) {
            System.out.println("Select a number: ");
            Scanner scanner = new Scanner(System.in);
            try {
                option = scanner.nextInt();
                if (option>0 && option<3) {
                    optionNotSelected = false;}
                else{
                    System.out.println("Invalid input");
                }
            } catch (java.util.InputMismatchException e) {
                System.out.println("Invalid input!");
            }
        }
        return option;

    }
    public static ArrayList getByCategory(){
        System.out.println("Current categories:");
        System.out.println(ProductCategory.getAllCategories());
        Scanner scanner = new Scanner(System.in);
        System.out.println("Category name: ");
        String myProductCategoryName = scanner.nextLine();
        ProductCategory myProductCategory = ProductCategory.getCategoryByName(myProductCategoryName);
        return Product.getAllProductsBy(myProductCategory);
    }

    public static Product checkAvailability(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Product to find: ");
        String productNameToFind = scanner.nextLine();
        Product myNewProduct =  new Product("book", 2,  new ProductCategory("Cat"));
        Iterator iterator = myNewProduct.getIterator();
        while (iterator.hasNext()) {
            Object object = iterator.next();
            Product product = (Product)object;
            if(product.getName().equals(productNameToFind)){
                return (Product) object;
            }
        }
        System.out.println("ProductNotFound");
        return null;

        }

        public static void addProductToBasket(Basket myBasket){
        System.out.println(getListOfAllProducts());
            System.out.println("Select name of the chosen product.");
            Product productToAdd = checkAvailability();
            myBasket.addProduct(productToAdd);
        }

        public static void seeYourBasket(Basket myBasket){
            Iterator iterator = myBasket.getIterator();
            System.out.println("Currently in your basket:");
            while (iterator.hasNext()){
                Object object = iterator.next();
                Product product = (Product)object;
                System.out.println(product);
            }

        }
    public static Product checkAvailabilityInBasket( Basket myBasket){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Product to remove(name): ");
        String productNameToFind = scanner.nextLine();
        Iterator iterator = myBasket.getIterator();
        while (iterator.hasNext()) {
            Object object = iterator.next();
            Product product = (Product)object;
            if(product.getName().equals(productNameToFind)){
                return (Product) object;
            }
        }
        System.out.println("ProductNotFound");
        return null;

    }


    public static void removeProductFromBasket(Basket myBasket){
        seeYourBasket(myBasket);
        Product productToRemove = checkAvailabilityInBasket(myBasket);
        myBasket.removeProduct(productToRemove);
    }

    public static void payForOrder(Basket myBasket){
        float totalPrice = 0;
        Order myOrder = new Order();
        Iterator iterator = myBasket.getIterator();
        while (iterator.hasNext()) {
            Object object = iterator.next();
            Product product = (Product)object;
            float price = product.getDefaultPrice();
            totalPrice += price;
            }
            System.out.println(String.format("Total price: %.2f",totalPrice));
        System.out.println("Confirm payment (y/n)");
        Scanner scanner = new Scanner(System.in);
        String confirmation = scanner.nextLine();
        if(confirmation.equals("y")){

        AbstractProcess payment = new PaymentProcess();
        payment.process(myOrder);}

        else {
            System.out.println("Invalid confirmation");
        }


    }
}


