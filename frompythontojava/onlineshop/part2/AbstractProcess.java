package frompythontojava.onlineshop.part2;

public abstract class AbstractProcess {

    public void process(Orderable item){
        stepBefore();
        action(item);
        stepAfter();
    }
    public void stepBefore(){
        System.out.println("Processing...");
    }

    protected void action(Orderable item){} //implement in the subclass

    public void stepAfter(){
        System.out.println("Process completed successfully:)");
    }


}
