package frompythontojava.onlineshop.part2;

public class Order implements Orderable {
    int ID;
    String status;
    static int counter;

    public Order(){
        this.ID = counter;
        this.status = "new";
        counter ++;
    }

    public String getStatus(){
        return this.status;
    }

    public int getID(){
        return this.ID;
    }

    @Override
    public boolean checkout() {
        this.status = "checked";
        return true;
    }

    @Override
    public boolean pay() {
        this.status = "payed";
        return true;
    }
}
