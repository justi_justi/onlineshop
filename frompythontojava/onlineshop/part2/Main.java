package frompythontojava.onlineshop.part2;

public class Main {

    public static void main(String[] args) {
	Order myOrder = new Order();
	System.out.println(myOrder.getStatus());
	AbstractProcess checkout = new CheckoutProcess();
	checkout.process(myOrder);
	System.out.println(myOrder.getStatus());
	AbstractProcess payment = new PaymentProcess();
	payment.process(myOrder);
	System.out.println(myOrder.getStatus());
	System.out.println(myOrder.getID());
	Order myOrder2 = new Order();
	System.out.println(myOrder2.getID());
	System.out.println(myOrder2.getStatus());
    }
}
